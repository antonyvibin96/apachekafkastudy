package kafka_basics.com.github.antonyvibin96.kafkabeginners;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.errors.WakeupException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

public class ConsumerDemoAssignAndSeek {
    public static String consumerGroup = "java_consumer_group_3";
    public static String topic = "java_topic_1";
    private Logger logger= LoggerFactory.getLogger(ConsumerDemoAssignAndSeek.class);

    public static void main(String[] args) {
        new ConsumerDemoAssignAndSeek().run();

    }
    public void run(){
        Properties properties = ConsumerDemo.getConsumerProperties();

        CountDownLatch latch=new CountDownLatch(1);
        Runnable consumerRunnable=new ConsumerRunnableAssignAndSeek(properties,latch);
        Thread myThread=new Thread(consumerRunnable);
        myThread.start();
        Runtime.getRuntime().addShutdownHook(new Thread(()->{
            logger.info("shutdown hook");
            ((ConsumerRunnableAssignAndSeek)consumerRunnable).shutdown();
            try {
                latch.await();
            } catch (InterruptedException e) {
                logger.info("Appication closed with latch awaits");
            }
        }

        ));
        try {
            latch.await();
        } catch (InterruptedException e) {
            logger.info("interrupted latch");
        }
    }

     class ConsumerRunnableAssignAndSeek implements Runnable{
        private CountDownLatch latch;
        private KafkaConsumer<String,String> kafkaConsumer;
        private  Logger logger= LoggerFactory.getLogger(ConsumerRunnableAssignAndSeek.class);
        public ConsumerRunnableAssignAndSeek(Properties properties, CountDownLatch latch){
            this.latch=latch;
            kafkaConsumer=new KafkaConsumer<String, String>(properties);

            String consumerApiKey="2jD9YeJvt5zIzrXQEh85I7Acl";
            String consumerApiSecret="GJBWsRu1SYG0Pq0wdb4KiTIsh4KLVDTCfVzir9mRY6iMIZHB5y";
            String bearerToken="AAAAAAAAAAAAAAAAAAAAACTiMgEAAAAAeg2ORYJjP35ld4raM6mDqM0ryu4%3DaaaY4UgEHipM5hMesxJ18XXtGHNJ4FqZm7o44PJS5wHzgAXSks";
            String accessToken="1177436888283541504-NU4kxGx7sC89Ybwr9pARM4M17CDj2T";
            String accessTokenSecret="KkVCxbTApXoNjb4Tf13BjmIR8PEiqGILItFq57TjKeSYE";
           // kafkaConsumer.subscribe(Arrays.asList(topic));

            TopicPartition topicPartition=new TopicPartition(topic,0);
            kafkaConsumer.assign(Arrays.asList(topicPartition));

            long offset=5L;

            kafkaConsumer.seek(topicPartition,offset);
        }

        @Override
        public void run() {
            try {
                int numberOfMessage=5;
                boolean keepReading=true;
                int numberofMessageReadSoFar=0;
                while (keepReading) {
                    ConsumerRecords<String, String> consumerRecord = kafkaConsumer.poll(Duration.ofMillis(1000));
                    for (ConsumerRecord<String, String> record : consumerRecord) {
                        numberofMessageReadSoFar++;
                        System.out.println("key : " + record.key() + "   value : " + record.value());
                        System.out.println("partition" + record.partition() + "   offset :  " + record.offset());
                          if(numberofMessageReadSoFar>=numberOfMessage){
                              keepReading=false;
                              break;
                          }
                    }
                }
                logger.info("Exiting while loop");
            }catch (WakeupException e){
                logger.info("Recieved Wake Up Call ");
            }finally {
                kafkaConsumer.close();
                latch.countDown();
            }

        }
        public void shutdown(){
            logger.info("Recieved shutdown call");
            kafkaConsumer.wakeup();

        }
    }
}
