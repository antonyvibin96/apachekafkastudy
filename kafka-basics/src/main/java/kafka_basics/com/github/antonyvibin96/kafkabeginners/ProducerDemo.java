package kafka_basics.com.github.antonyvibin96.kafkabeginners;


import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.protocol.types.Field;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;

public class ProducerDemo {
    static String  bootstrapServer="127.0.0.1:9092";
    public static void main(String[] args) {

       KafkaProducer<String,String> kafkaProducer=createProducer();
       producerSend(kafkaProducer,"java_topic_1","Hello World!");
       producerSend(kafkaProducer,"java_topic_1","Hdfvldf");
       kafkaProducer.close();
    }

    public static void producerSend(KafkaProducer<String,String> kafkaProducer,String key,String value){
        ProducerRecord<String,String> producerRecord=new ProducerRecord<>(key,value);
        kafkaProducer.send(producerRecord);
    }
    public static KafkaProducer<String,String> createProducer(){
        Properties producerProperties=new Properties();
        producerProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,bootstrapServer);
        producerProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        producerProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,StringSerializer.class.getName() );

        //safe producer
        producerProperties.put(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG,"true");
        producerProperties.put(ProducerConfig.ACKS_CONFIG, "all");
        producerProperties.put(ProducerConfig.RETRIES_CONFIG, Integer.toString(Integer.MAX_VALUE));
        producerProperties.put(ProducerConfig.MAX_IN_FLIGHT_REQUESTS_PER_CONNECTION,"5");


        //compression

        producerProperties.put(ProducerConfig.COMPRESSION_TYPE_CONFIG,"snappy");


        // high through put settings
        // compression

        producerProperties.put(ProducerConfig.LINGER_MS_CONFIG, 20);
        producerProperties.put(ProducerConfig.BATCH_SIZE_CONFIG,Integer.toString(32*1024));

        KafkaProducer<String,String> kafkaProducer=new KafkaProducer<String, String>(producerProperties);
        return kafkaProducer;
    }
}
