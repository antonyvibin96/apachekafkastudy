package kafka_basics.com.github.antonyvibin96.kafkabeginners;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;

public class ConsumerDemoGroup {
    static Logger logger= LoggerFactory.getLogger(ConsumerDemo.class);
    static String bootStrapServer="127.0.0.1:9092";
    static  String consumerGroup="java_consumer_group_1";
    static String topic="java_topic_1";
    public static void main(String[] args) {
        Properties properties=ConsumerDemo.getConsumerProperties(consumerGroup);

        KafkaConsumer<String,String> kafkaConsumer=new KafkaConsumer<String, String>(properties);

        kafkaConsumer.subscribe(Arrays.asList(topic));
        while (true){
            ConsumerRecords<String, String> consumerRecord= kafkaConsumer.poll(Duration.ofMillis(1000));
            for(ConsumerRecord<String,String> record:consumerRecord){
                System.out.println("key : "+record.key()+"   value : "+record.value());
                System.out.println("partition"+record.partition()+"   offset :  "+record.offset());
            }
        }
    }
}

