package kafka_basics.com.github.antonyvibin96.kafkabeginners;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.errors.WakeupException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

public class ConsumerDemoThread {
    public static String consumerGroup = "java_consumer_group_2";
    public static String topic = "java_topic_1";
    private  Logger logger= LoggerFactory.getLogger(ConsumerDemoThread.class);

    public static void main(String[] args) {
        new ConsumerDemoThread().run();

    }
    public void run(){
        Properties properties = ConsumerDemo.getConsumerProperties(consumerGroup);

        CountDownLatch latch=new CountDownLatch(1);
        Runnable consumerRunnable=new ConsumerRunnable(properties,latch);
        Thread myThread=new Thread(consumerRunnable);
        myThread.start();
        Runtime.getRuntime().addShutdownHook(new Thread(()->{
       logger.info("shutdown hook");
            ((ConsumerRunnable) consumerRunnable).shutdown();
            try {
                latch.await();
            } catch (InterruptedException e) {
                logger.info("Appication closed with latch awaits");
            }
        }

        ));
        try {
            latch.await();
        } catch (InterruptedException e) {
            logger.info("interrupted latch");
        }
    }

    private class ConsumerRunnable implements Runnable{
        private CountDownLatch latch;
        private KafkaConsumer<String,String> kafkaConsumer;
        private  Logger logger= LoggerFactory.getLogger(ConsumerRunnable.class);
        public ConsumerRunnable(Properties properties, CountDownLatch latch){
            this.latch=latch;
            kafkaConsumer=new KafkaConsumer<String, String>(properties);
            kafkaConsumer.subscribe(Arrays.asList(topic));
        }

        @Override
        public void run() {
            try {
                while (true) {
                    ConsumerRecords<String, String> consumerRecord = kafkaConsumer.poll(Duration.ofMillis(1000));
                    for (ConsumerRecord<String, String> record : consumerRecord) {
                        System.out.println("key : " + record.key() + "   value : " + record.value());
                        System.out.println("partition" + record.partition() + "   offset :  " + record.offset());
                    }
                }
            }catch (WakeupException e){
                logger.info("Recieved Wake Up Call ");
            }finally {
                kafkaConsumer.close();
                latch.countDown();
            }

        }
        public void shutdown(){
            logger.info("Recieved shutdown call");
          kafkaConsumer.wakeup();

        }
    }
}

