package kafka_basics.com.github.antonyvibin96.kafkabeginners;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;

public class ConsumerDemo {
    public static Logger logger= LoggerFactory.getLogger(ConsumerDemo.class);
    public static String bootStrapServer="127.0.0.1:9092";
    public static  String consumerGroup="java_consumer_group_1";
    public static String topic="java_topic_1";
    public static void main(String[] args) {
     Properties properties=getConsumerProperties(consumerGroup);

        KafkaConsumer<String,String> kafkaConsumer=new KafkaConsumer<String, String>(properties);

        kafkaConsumer.subscribe(Arrays.asList(topic));
        while (true){
            ConsumerRecords<String, String> consumerRecord= kafkaConsumer.poll(Duration.ofMillis(1000));
            for(ConsumerRecord<String,String> record:consumerRecord){
                logger.info("key : "+record.key()+"   value : "+record.value());
                logger.info("partition",record.partition()+"   offset :  "+record.offset());
            }
        }
    }
    public static Properties getConsumerProperties(String groupId){
        Properties properties=new Properties();
        properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,bootStrapServer);
        properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,StringDeserializer.class.getName());
        properties.put(ConsumerConfig.GROUP_ID_CONFIG,groupId);
        properties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG,"earliest");
        return properties;
    }
    public static Properties getConsumerProperties(){
        Properties properties=new Properties();
        properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,bootStrapServer);
        properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,StringDeserializer.class.getName());
        properties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG,"earliest");
        return properties;
    }
}
