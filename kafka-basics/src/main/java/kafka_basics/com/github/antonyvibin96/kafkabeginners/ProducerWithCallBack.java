package kafka_basics.com.github.antonyvibin96.kafkabeginners;

import org.apache.kafka.clients.producer.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProducerWithCallBack {
    static String  bootstrapServer="127.0.0.1:9092";
    static Logger logger= LoggerFactory.getLogger(ProducerWithCallBack.class);
    public static void main(String[] args) {

        KafkaProducer<String,String> kafkaProducer=ProducerDemo.createProducer();



        for(int i=0;i<10;i++) {
            ProducerRecord<String,String> producerRecord=new ProducerRecord<>("java_topic_1","key_"+i,"Hello World "+i);
            kafkaProducer.send(producerRecord, new Callback() {
                @Override
                public void onCompletion(RecordMetadata recordMetadata, Exception e) {
                    if (e == null) {
                        logger.info("message send");
                        logger.info("Topic : " + recordMetadata.topic() + "\n" +
                                "Partion :" + recordMetadata.partition() + "\n" +
                                "Offset : " + recordMetadata.offset() + "\n" +
                                "Time stamp : " + recordMetadata.timestamp() + "\n" +
                                "All : " + recordMetadata.toString()
                        );
                    } else {
                        logger.info("error " + e.getStackTrace());
                    }
                }
            });
        }
        kafkaProducer.close();
    }
}
