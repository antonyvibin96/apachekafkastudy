package kafkatwitter.com.github.antonyvibin96.twiiterAPI;

import com.google.common.collect.Lists;
import com.twitter.hbc.ClientBuilder;
import com.twitter.hbc.core.Client;
import com.twitter.hbc.core.Constants;
import com.twitter.hbc.core.Hosts;
import com.twitter.hbc.core.HttpHosts;
import com.twitter.hbc.core.endpoint.StatusesFilterEndpoint;
import com.twitter.hbc.core.processor.StringDelimitedProcessor;
import com.twitter.hbc.httpclient.auth.Authentication;
import com.twitter.hbc.httpclient.auth.OAuth1;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import kafka_basics.com.github.antonyvibin96.kafkabeginners.ProducerDemo;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class TwitterProducer {
    String consumerApiKey="2jD9YeJvt5zIzrXQEh85I7Acl";
    String consumerApiSecret="GJBWsRu1SYG0Pq0wdb4KiTIsh4KLVDTCfVzir9mRY6iMIZHB5y";
    String bearerToken="AAAAAAAAAAAAAAAAAAAAACTiMgEAAAAAeg2ORYJjP35ld4raM6mDqM0ryu4%3DaaaY4UgEHipM5hMesxJ18XXtGHNJ4FqZm7o44PJS5wHzgAXSks";
    String accessToken="1177436888283541504-NU4kxGx7sC89Ybwr9pARM4M17CDj2T";
    String accessTokenSecret="KkVCxbTApXoNjb4Tf13BjmIR8PEiqGILItFq57TjKeSYE";
    static Logger logger= LoggerFactory.getLogger(TwitterProducer.class);
    List<String> terms = Lists.newArrayList("bitcoin");
    public TwitterProducer() {
    }

    public void  run(){
        logger.info("Application started");
        BlockingQueue<String> msgQueue = new LinkedBlockingQueue<String>(100);
        Client twitterClient=createTwitterClient(msgQueue);
        twitterClient.connect();


        KafkaProducer<String,String> kafkaProducer= ProducerDemo.createProducer();

        Runtime.getRuntime().addShutdownHook(new Thread(()-> {
            logger.info("Application ShutDown ");
            twitterClient.stop();
               kafkaProducer.close();

        }));

        while (!twitterClient.isDone()) {
            String msg = null;
            try {
                msg = msgQueue.poll(5, TimeUnit.SECONDS);

            } catch (InterruptedException e) {
                e.printStackTrace();
                twitterClient.stop();
                break;
            }

            if(msg!=null){
                logger.info(msg);
                ProducerRecord<String,String> producerRecord=new ProducerRecord<>("twitter_topic_2",null,msg);
                kafkaProducer.send(producerRecord, new Callback() {
                    @Override
                    public void onCompletion(RecordMetadata recordMetadata, Exception e) {
                        if(e!=null){
                            logger.error("Error Occured !!"+e.getMessage());
                        }
                    }
                });
            }
        }
        logger.info("Application Ended ");

    }
    public Client createTwitterClient( BlockingQueue<String> msgQueue){
        /** Declare the host you want to connect to, the endpoint, and authentication (basic auth or oauth) */
        Hosts hosebirdHosts = new HttpHosts(Constants.STREAM_HOST);
        StatusesFilterEndpoint hosebirdEndpoint = new StatusesFilterEndpoint();
// Optional: set up some followings and track terms
        List<Long> followings = Lists.newArrayList(1234L, 566788L);
       // hosebirdEndpoint.followings(followings);
        hosebirdEndpoint.trackTerms(terms);

// These secrets should be read from a config file
        Authentication hosebirdAuth = new OAuth1(consumerApiKey, consumerApiSecret, accessToken, accessTokenSecret);

        ClientBuilder builder = new ClientBuilder()
                .name("Hosebird-Client-01")                              // optional: mainly for the logs
                .hosts(hosebirdHosts)
                .authentication(hosebirdAuth)
                .endpoint(hosebirdEndpoint)
                .processor(new StringDelimitedProcessor(msgQueue));
//                .eventMessageQueue(eventQueue);                          // optional: use this if you want to process client events

        Client hosebirdClient = builder.build();
        return hosebirdClient;

    }
    public static void main(String[] args) {
        new TwitterProducer().run();
    }
}
