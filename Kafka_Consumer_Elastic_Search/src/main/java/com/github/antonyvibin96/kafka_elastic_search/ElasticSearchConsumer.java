package com.github.antonyvibin96.kafka_elastic_search;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;

public class ElasticSearchConsumer {

    private static String elastic_hostName="localhost";
    private static String elastic_port="9200";



    public RestHighLevelClient createRestClient(){
        RestHighLevelClient client = new RestHighLevelClient(
                RestClient.builder(
                        new HttpHost("localhost", 9200, "http"),
                        new HttpHost("localhost", 9201, "http")));

        return client;
    }
    public static void main(String[] args) {

    }
}
